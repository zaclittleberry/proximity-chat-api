# proximity-chat-api

This is a proximity chat server. By default it runs on http://localhost:9081.

It provides a REST and WebSocket api for User and Message information.

See a live version running at https://proximity-chat-api.herokuapp.com

## To Run

 `cp docker/variables.env.example docker/variables.env` with any changes for database credentials

`docker-compose up` to run the server

## To Deploy (tested with Heroku)

Make sure to add a PostgreSQL attachment (which will add a `DATABASE_URL` config var)

Also make sure to set the following config vars:

`CORS_ORIGIN`: `*` - Whatever your frontend url will be, or `*`
`DATABASE_URL_ADD_SSL_TRUE`: `TRUE` - adds dialectOptions to sequelize to fix a node-postgres regression
`DB_DESTROY_DATABASE_RESTRUCTURE`: `DB_DESTROY_DATABASE_RESTRUCTURE` - only add this if you want the database to forcibly drop tables and restructure itself to the ORM config (necessary until migrations are generated)
`SELF_HOST`: `proximity-chat-api.herokuapp.com` - The domain where the api is located. Allows swagger documentation to work.

## Documentation

Swagger/OpenAPI interface documentation is located at the `/documentation` route of where the api is hosted. For the live running version, this is https://proximity-chat-api.herokuapp.com/documentation


## TODO:

Add Messaging functionality. Currently only supports Users
