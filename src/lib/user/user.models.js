const Joi = require('joi');

module.exports = {
  db: (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      secret: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        unique: true,
      },
      name: DataTypes.STRING,
      coordX: DataTypes.INTEGER,
      coordY: DataTypes.INTEGER,
    });

    return User;
  },
  createUser: Joi.object().keys({
    name: Joi.string().required(),
  }),
  updateUser: Joi.object().keys({
    id: Joi.string().required(),
    name: Joi.string().required(),
    secret: Joi.string().guid().required(),
    coordX: Joi.number().integer().min(0).max(100).required(),
    coordY: Joi.number().integer().min(0).max(100).required(),
    createdAt: Joi.string(),
    updatedAt: Joi.string(),
  }),
};
