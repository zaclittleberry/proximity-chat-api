module.exports = {
  routes: (models) => {
    const controllers = require('./user.controllers')(models);
    const userModels = require('./user.models');
    return [
      {
        method: 'GET',
        path: '/users',
        handler: controllers.getUsers,
        config: {
          description: 'Get Users',
          notes: 'Get the users from the database',
          tags: ['api', 'Users'],
        }
      },
      {
        method: 'POST',
        path: '/ws/getUserEvents',
        handler: controllers.getUserEvents,
        config: {
          payload: { output: "data", parse: true, allow: "application/json" },
          plugins: { websocket: { only: true, autoping: 30 * 1000 } },
          description: 'Get WS User Events',
          notes: 'Get live user events',
          tags: ['api', 'WebSockets', 'Users'],
        },
      },
      {
        method: 'POST',
        path: '/users',
        handler: controllers.createUser,
        options: {
          description: 'Create User',
          notes: 'initialize user in the system',
          tags: ['api', 'Users'],
          validate: {
            payload: userModels.createUser,
          }
        },
      },
      {
        method: 'PUT',
        path: '/users',
        handler: controllers.updateUser,
        options: {
          description: 'Update User',
          notes: 'user in the system',
          tags: ['api', 'Users'],
          validate: {
            payload: userModels.updateUser,
          }
        },
      },
    ];
  },
};
