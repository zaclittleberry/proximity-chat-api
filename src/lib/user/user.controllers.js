module.exports = (models) => {
  const Boom = require('@hapi/boom');

  const EventEmitter = require('events');

  class UserEmitter extends EventEmitter {}
  const userEmitter = new UserEmitter();

  return {
    getUserEvents: async function(request, h) {
      console.log('client attached to getUserEvents');
      let { ws } = request.websocket();

      let initialUsers;
      try {
        initialUsers = await getUsers();
      } catch (e) {
        throw Boom.badImplementation('Error during getUsers', e);
      }

      ws.send(
        JSON.stringify({ cmd: "HELLO", arg: "OPEN", users: initialUsers})
      );

      let sendWsResponse = (params) => {
        console.log('event received');
        ws.send(JSON.stringify({users:params}));
      };

      userEmitter.on('event', sendWsResponse);

      ws.on('close', () => {
        console.log('ws closed');
        userEmitter.off('event', sendWsResponse);
      });

      return "";
    },
    getUsers: async function(request, h) {
      let response;
      try {
        response = await getUsers();
      } catch (e) {
        throw Boom.badImplementation('Error during User.findAll', e);
      }
      return response;
    },
    createUser: async function(request, h) {
      let response;
      try {
        response = await models.User.create({
          name: request.payload.name,
          coordX: randomNum(0,100),
          coordY: randomNum(0,100),
        });
      } catch (e) {
        throw Boom.badImplementation('Error during User.create', e);
      }

      try {
        // emit an event to send clients updated Users
        const users = await getUsers();
        console.log('sending event');
        userEmitter.emit('event', users);
      } catch (e) {
        throw Boom.badImplementation('Error during userEmitter.emit', e);
      }

      return h.response(response);

    },
    updateUser: async function(request, h) {
      let response;
      // shouldn't allow updating id or secret. only name and coords
      let updatedUser = {
        name: request.payload.name,
        coordX: request.payload.coordX,
        coordY: request.payload.coordY,
      };
      try {
        response = await models.User.update(updatedUser, {
          where: {
            secret: request.payload.secret,
          }
        });
      } catch (e) {
        throw Boom.badImplementation('Error during User.update', e);
      }

      if (response[0] == 0) { // if number of affected rows is none
        throw Boom.notFound('No user');
      }

      try {
        // emit an event to send clients updated Users
        const users = await getUsers();
        console.log('sending event');
        userEmitter.emit('event', users);
      } catch (e) {
        throw Boom.badImplementation('Error during userEmitter.emit', e);
      }

      return h.response(response);

    },
  };

  function getUsers() {
    return models.User.findAll({
      attributes: { exclude: ['secret'] }
    });
  }

  function randomNum(min, max) {
    return Math.floor(
      Math.random() * (max - min + 1) + min
    );
  }
};
