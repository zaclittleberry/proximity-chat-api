'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');

const Boom = require('@hapi/boom');
const {Sequelize, DataTypes} = require('sequelize');
const HAPIWebSocket = require("hapi-plugin-websocket");

module.exports = (async() => {

  await ensureEnvironmentVariables();

  const server = await newHapiServer();

  const sequelize = await newSequelize();

  const modules = require('./lib/modules');

  // sequelize modified by reference, returns models for injecting elsewhere
  const models = await defineAndAssociateModels(sequelize, modules);

  // server modified by reference to register routes, nothing returned
  await registerModulesRoutes(server, models, modules);

  // Register HAPIWebSocket so routes can use wss://
  await server.register(HAPIWebSocket);

  await registerSwaggerDocs(server);

  try {
    server.start();
    console.info('server running at ' + process.env.SELF_HOST);
  } catch(err) {
    console.error(err);
  }

  return {
    server: server,
  };
})();

function ensureEnvironmentVariables() {
  const envVars = [
    'CORS_ORIGIN',
    'SELF_HOST',
    'DATABASE_URL',
  ];

  for (let envVar of envVars) {
    if (!process.env[envVar]) {
      console.error(`Error: Make sure you have ${envVar} in your environment variables.`);
    }
  }
}

function newHapiServer() {
  const server = new Hapi.Server({
    port: process.env.PORT || 8081,
    routes: {cors: {
      additionalHeaders: ['access-control-allow-origin'],
      exposedHeaders: ['Content-Location'],
      origin: [process.env.CORS_ORIGIN],
    }}
  });
  return server;
}

function newSequelize() {
  let sequelize;
  try {
    let dialectOptions = {};
    if (process.env.DATABASE_URL_ADD_SSL_TRUE) {
      dialectOptions.ssl = {
        require: true,
        rejectUnauthorized: false
      };
    }
    console.log('dialectOptions', dialectOptions)
    sequelize = new Sequelize(process.env.DATABASE_URL, {
      pool: {
        log: true,
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
      logging: false,
      dialectOptions: dialectOptions,
    });
  } catch (err) {
    console.error('Sequelize init error:');
    console.error(err);
  }
  return sequelize;
}

async function defineAndAssociateModels(sequelize, modules) {
  let models = {}
  // define the models of all of our modules
  for (let mod of modules) {
    let modelsFile;
    try {
      modelsFile = require(`./lib/${mod}/${mod}.models.js`);
      if (modelsFile.db) {
        let model = modelsFile.db(sequelize, DataTypes);
        models[model.name] = model;
      }
    } catch(err) {
      console.log(err);
      console.log(`module ${mod} did not have a models file`);
    }
  }
  // now that all the models are loaded, run associations
  Object.keys(models).forEach(function(modelName) {
    if (models[modelName].associate) {
      models[modelName].associate(models);
    }
  });
  models.sequelize = sequelize;

  try {
    if (process.env['DB_DESTROY_DATABASE_RESTRUCTURE'] === 'DB_DESTROY_DATABASE_RESTRUCTURE') {
      // NOTE: This will wipe/forcibly restructure a database. ONLY USE FOR DEV.
      await sequelize.sync({force: true});
    } else {
      await sequelize.sync();
    }
  } catch (e) {
    console.log('Error during sync:', e);
  }

  return models;
}

async function registerModulesRoutes(server, models, modules) {
  // Build the routes of all our modules, injecting the models into each
  for (let mod of modules) {
    let routesFile;
    try {
      routesFile = require(`./lib/${mod}/${mod}.routes.js`);
      if(routesFile.routes) {
        await server.route(routesFile.routes(models));
      }
    } catch(err) {
      console.error(err);
      console.error(`module ${mod} did not have a routes file or hapi failed to register them`);
    }
  }

  server.route({
    method: 'GET',
    path: '/',
    handler: function (request, h) {
      return h
        .response({status: 'up'});
    }
  });
}

async function registerSwaggerDocs(server) {
  const swaggerOptions = {
    host: process.env.SELF_HOST,
    info: {
      title: 'API Documentation',
      version: "1.0",
    },
    grouping: 'tags',
  };

  try {
    await server.register([Inert, Vision, {
      'plugin': HapiSwagger,
      'options': swaggerOptions
    }]);
  } catch (err) {
    console.error(err);
  }
}
